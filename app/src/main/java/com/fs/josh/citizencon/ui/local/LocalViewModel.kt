package com.fs.josh.citizencon.ui.local

import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class LocalViewModel : ViewModel() {
    private lateinit var str: MutableLiveData<String>

    private var thenMillis: Long? = null
    private var nowMillis: Long? = null

    init {
        val c = Calendar.getInstance(Locale.UK)
        c.set(2019, Calendar.OCTOBER, 10, 9, 0, 0)
        thenMillis = c.timeInMillis

        val _c = Calendar.getInstance(Locale.UK)
        nowMillis = _c.timeInMillis
    }

    fun getTimeLeft(): LiveData<String> {
        if (!::str.isInitialized) {
            str = MutableLiveData()
            set()
        }
        return str
    }

    private fun set() {
        object : CountDownTimer(thenMillis!! - nowMillis!!, 1000) {
            // TODO notification
            override fun onFinish() {
                Log.d("TICKER_tA", "Finished")
            }

            override fun onTick(millisUntilFinished: Long) {
                val string = String.format("%02d days %02d hours, %02d minutes",
                        TimeUnit.MILLISECONDS.toDays(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)))
                str.postValue(string)
            }
        }.start()
    }
}