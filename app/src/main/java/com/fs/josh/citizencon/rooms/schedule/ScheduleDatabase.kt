package com.fs.josh.citizencon.rooms.schedule

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [ScheduleRoom::class], version = 1)
abstract class ScheduleDatabase : RoomDatabase() {
    abstract fun scheduleDao(): ScheduleDao
}