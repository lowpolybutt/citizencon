package com.fs.josh.citizencon.ui.schedule

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class SchedulePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private val schedules by lazy { arrayListOf(Pair(ScheduleStageOne(), "Stage One"), Pair(SchedulePageTwo(), "Stage Two"))}

    override fun getCount(): Int = 2
    override fun getItem(position: Int): Fragment = schedules[position].first
    override fun getPageTitle(position: Int): CharSequence? = schedules[position].second
}