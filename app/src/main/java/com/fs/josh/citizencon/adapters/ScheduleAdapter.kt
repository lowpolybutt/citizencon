package com.fs.josh.citizencon.adapters

import android.content.Context
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.fs.josh.citizencon.R
import com.fs.josh.citizencon.rooms.schedule.ScheduleRoom
import java.lang.StringBuilder

class ScheduleAdapter(private val ctx: Context) : RecyclerView.Adapter<ScheduleAdapter.ItemHolder>() {

    private var schedule = emptyList<ScheduleRoom>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder = ItemHolder(parent)

    override fun onBindViewHolder(holder: ItemHolder, position: Int) = holder.bind(schedule[position])

    override fun getItemCount(): Int = schedule.size

    fun updateList(list: List<ScheduleRoom>) {
        this.schedule = list
        notifyDataSetChanged()
    }

    // TODO fix height calculation, seems to be off
    inner class ItemHolder(vg: ViewGroup) : RecyclerView.ViewHolder(LayoutInflater.from(vg.context).inflate(R.layout.schedule_item_holder, vg, false)) {
        private val tv: TextView = itemView.findViewById(R.id.schedule_event_title)
        private val description: TextView = itemView.findViewById(R.id.schedule_event_description)
        private val speakers: TextView = itemView.findViewById(R.id.schedule_event_speakers)
        private val cv: CardView = itemView.findViewById(R.id.schedule_item_card)
        internal fun bind(event: ScheduleRoom) {
            tv.text = event.title
            description.text = event.description
            val cvParams = cv.layoutParams
            val scale = ctx.resources.displayMetrics.density
            val px = (175.0f * scale + 0.5f)
            cvParams.height = (px * event.length).toInt()
            cv.layoutParams = cvParams
            val sb = StringBuilder()
            if (event.speakers.isNotEmpty()) {
                if (event.speakers.size == 1) {
                    sb.append(event.speakers[0])
                } else {
                    event.speakers.forEachIndexed { index, s ->
                        if (index != event.speakers.size - 1) {
                            sb.apply {
                                append(s)
                                append(", ")
                            }
                        } else {
                            sb.append(s)
                        }
                    }
                }
            }
            speakers.text = sb.toString()
        }
    }
}