package com.fs.josh.citizencon.ui.schedule

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.room.Room
import com.fs.josh.citizencon.rooms.schedule.ScheduleDatabase
import com.fs.josh.citizencon.rooms.schedule.ScheduleRoom
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.RuntimeException

class ScheduleViewModel : ViewModel() {

    private val db by lazy { FirebaseFirestore.getInstance() }

    private lateinit var schedule: MutableLiveData<List<ScheduleRoom>>

    private val events = ArrayList<ScheduleRoom>()

    private lateinit var stage: String

    fun getBasicEvents(stage: String): LiveData<List<ScheduleRoom>> {
        this.stage = stage
        if (!::schedule.isInitialized) {
            schedule = MutableLiveData()
            populateList()
        }
        return schedule
    }

    private fun populateList() {
        db.collection(stage).get()
                .addOnCompleteListener { t ->
                    if (t.isSuccessful) {
                        t.result?.forEachIndexed { i, qs ->
                            qs.reference.addSnapshotListener { documentSnapshot, firebaseFirestoreException ->
                                if (firebaseFirestoreException != null) {
                                    Log.e("EVENT_TAG", "Error fetching basic schedule")
                                } else {
                                    val d = documentSnapshot?.toObject(ScheduleRoom::class.java)
                                    if (d != null) {
                                        if (events.size > i ) {
                                            events.removeAt(i)
                                            events.add(i, d)
                                        } else {
                                            events.add(i, d)
                                        }
                                        val sorted = events.sortedWith(compareBy { it.id })
                                        schedule.postValue(sorted)
                                    } else {
                                        throw RuntimeException("Conversion to POJO failed")
                                    }
                                }
                            }
                        }
                    }
                }
    }
}