package com.fs.josh.citizencon

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import com.fs.josh.citizencon.ui.main.MainFragment
import com.google.android.material.navigation.NavigationView
import com.google.firebase.FirebaseApp

class MainActivity : AppCompatActivity() {

    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        FirebaseApp.getInstance()

        drawerLayout = findViewById(R.id.activity_main_drawer_layout)

        val navView = findViewById<NavigationView>(R.id.activity_main_nav_drawer)
        navView.setNavigationItemSelectedListener { item ->
            item.isChecked = true
            drawerLayout.closeDrawers()
            when (item.itemId) {
                R.id.nav_drawer_schedule -> findNavController(R.id.activity_main_nav_host_fragment).navigate(R.id.scheduleFragment)
                R.id.nav_drawer_local -> findNavController(R.id.activity_main_nav_host_fragment).navigate(R.id.localFragment)
            }
            true
        }

        val tb: Toolbar = findViewById(R.id.activity_main_toolbar)
        setSupportActionBar(tb)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.baseline_menu_white_24)
        }
    }

    override fun onSupportNavigateUp(): Boolean = findNavController(R.id.activity_main_nav_host_fragment).navigateUp()

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                drawerLayout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
